export default class Service {
  constructor(login, options) {
    if (this.constructor === Service) {
      throw new TypeError('Abstract class "Service" cannot be instantiated directly.');
    }

    if (undefined === this.send) {
      throw new TypeError('Classes extending the "Service" abstract class must implement the "send()" method.');
    }

    this._login = login;

    if ('undefined' === typeof options) {
      this._options = { };
    } else {
      this._options = options;
    }
  }

  limit() {
    return 0;
  }

  process(msg) {
    return msg;
  }
}
