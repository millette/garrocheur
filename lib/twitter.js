import Service from './service';
import md2plain from './markdown-to-plain';
import _Imp from 'twitter';

export default class Twitter extends Service {
  constructor(login, Imp) {
    super(login, { limit: 140 });
    this._servicename = 'twitter';
    this._Imp = 'undefined' === typeof Imp ? _Imp : Imp;
    this._imp = new this._Imp(login);
  }

  send(msg, aspects, cb) {
    this._imp.post('statuses/update', {status: msg},
      function (error, tweet) { // last arg: , response
        if (error) {
          return cb(error);
        }
        cb(null, this._servicename, [Date.now(), this._servicename, tweet, '']);
      }.bind(this)
    );
  }

  process(msg) {
    return md2plain(msg);
  }

  limit() {
    return this._options.limit;
  }
}
