import Twitter from './twitter';
import Diaspora from './diaspora';

//TODO: document with jsdocs/esdocs

let plugins = {
  twitter: {
    Ctor: Twitter
  },
  diaspora: {
    Ctor: Diaspora
  }
};

export default class Garrocheur {
  constructor(service, login, options, Imp) {
    this._services = {};
    this._users = {};
    this._aspect = [Garrocheur.PUBLIC];
    this._aspects = {};
    this._message = false;
    this._mimetype = 'text/plain'; // text/markdown; charset=UTF-8

    if ('undefined' !== typeof service && 'undefined' !== typeof login) {
      this.service(service, login, options, Imp);
    }
  }

  service(service, login, options, Imp) {
    if ('undefined' === typeof plugins[service]) {
      throw new Error('Service non-supporté.');
    }
    if ('undefined' === typeof this._services[service]) {
      this._services[service] = new plugins[service].Ctor(login, options, Imp);
    }
  }

  _isService(service, what) {
    let msg = 'Le service ' + service + ' n\'existe pas';
    if ('undefined' === typeof this._services[service]) {
      if ('undefined' !== typeof what) {
        msg += ' pour ajouter ' + what;
      }
      throw new Error(msg + '.');
    }
  }

  _map(key, what, service) {
    if ('string' === typeof service) {
      service = [what, service];
    }

    this._isService(service[1], what);

    if ('undefined' === typeof this[key][what]) {
      this[key][what] = [service];
    } else {
      this[key][what].push(service);
    }
  }

  mapUser(name, service) {
    this._map('_users', name, service);
  }

  mapUsers(service) {
    let users, msg = this._message;
    this._isService(service);
    users = Object.keys(this._users);
    if (!this._message || !users.length) {
      return msg;
    }

    users.forEach(function (v) {
      this._users[v].forEach(function (v2) {
        if (service === v2[1] && v !== v2[0]) {
          msg = msg.replace(
            new RegExp('@' + v + '(\\b)', 'g'),
            '@' + v2[0] + '$1'
          );
        }
      });
    }.bind(this));
    return msg;
  }

  mappedAspects(service) {
    let ret = [];
    this._isService(service);
    Object.keys(this._aspects).forEach(function (v) {
      this._aspects[v].forEach(function (v2) {
        if (service === v2[1]) {
          ret.push(v2[0]);
        }
      });
    }.bind(this));

    return ret;
  }

/* TODO
  mappedHashtags(service) {
  }

  mapHashtag(hashtag, service) {
  }
*/

  mapAspect(aspect, service) {
    if ('string' !== typeof aspect ||
      ('string' !== typeof service && 'object' !== typeof service)) {
      throw new Error('mapAspect veut 2 arguments: aspect et service.');
    }
    this._map('_aspects', aspect, service);
  }

  message(message, mimetype) {
    let parts;

    this._message = message;
    if ('undefined' === typeof mimetype) {
      this._mimetype = 'text/plain; charset=UTF-8';
    } else {
      parts = mimetype.split('/');
      this._mimetype = 1 === parts.length ?
        'text/' + mimetype :
        mimetype;
      if (-1 === mimetype.indexOf('charset=')) {
        this._mimetype += '; charset=UTF-8';
      }
    }
  }

  aspects(aspects) {
    this._aspect = 'string' === typeof aspects ? [aspects] : aspects;
  }

  public(aspect, service) {
    if ('undefined' === typeof service) {
      this.mapAspect(Garrocheur.PUBLIC, aspect);
    } else {
      this.mapAspect(aspect, [Garrocheur.PUBLIC, service]);
    }
  }

  _send(cb, service) {
    let msg, ret, limit = this._services[service].limit(),
      ok = false, a = this.mappedAspects(service);

    if (!a.length) {
      return cb(`error: no known aspects on ${service}.`);
    }

    this._aspect.forEach(function (v) {
      ok = ok || -1 !== a.indexOf(v);
    });

    if (!ok) {
      return cb(`error sending to ${service}, no aspects to send to.`);
    }

    msg = this.mapUsers(service);
    if (0 === this._mimetype.indexOf('text/markdown')) {
      msg = this._services[service].process(msg);
    }

    if (limit) {
      ret = [
        Date.now(), service,
        msg.slice(0, limit).trim(),
        msg.slice(limit).trim()
      ];

    } else {
      ret = [Date.now(), service, msg, ''];
    }

    this._services[service].send(ret[2], this._aspect, cb);
  }

  _ready() {
    let services = Object.keys(this._services);
    if (!services.length) {
      throw new Error('Aucun service défini pour envoyer le message.');
    }

    if (!this._message) {
      throw new Error('Aucun message à envoyer.');
    }

    return services;
  }

  send(cb) {
    this._ready().forEach(this._send.bind(this, cb));
  }
}

Garrocheur.PUBLIC = 'public';
