import Service from './service';
import _Imp from 'node-diaspora';

export default class Diaspora extends Service {
  constructor(login, server, Imp) {
    super(login, { server: server });
    this._servicename = 'diaspora';
    this._Imp = 'undefined' === typeof Imp ? _Imp : Imp;
    this._imp = new this._Imp({user: login.username, password: login.password, pod: server});
  }

  _connect(cb) {
    try {
      this._imp.connect(cb);
      // do more...
    } catch (e) {
      return cb(e);
    }
  }

  send(msg, aspects, cb) {
    this._connect(function (errc/*, suc*/) {
      if (errc) {
        return cb(errc, this._servicename);
      }
      this._imp.postStatusMessage(msg, aspects[0], function (err, ok) {
        //FIXME reformat the output
        cb(err, this._servicename, ok);
      }.bind(this));
    }.bind(this));
  }
}
