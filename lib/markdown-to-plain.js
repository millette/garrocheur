import marked from 'marked';
import strip from 'htmlstrip-native';

class Renderer {
  _plain(text) {
    return strip.html_strip(text).replace('\n', ' ').trim();
  }

  _href(href, text) {
    return `${href} (${text})`;
  }

  list(text) {
    return this._plain(text.slice(0, -2)); // remove ", " suffix
  }

  listitem(text) {
    return this._plain(text) + ', ';
  }

  paragraph(text) {
    return this._plain(text);
  }

  strong(text) {
    return `*${text}*`;
  }

  em(text) {
    return this.strong(text);
  }

  code(text) {
    return this._plain(text);
  }

  blockquote(text) {
    return this._plain(text);
  }

  heading(text) {
    return this._plain(text) + ' ';
  }

  del(text) {
    return `-${text}-`;
  }

  link(href, title, text) {
    return this._href(href, text);
  }

  image(href, title, alt) {
    return this._href(href, alt);
  }

  text(text) {
    return text;
  }

/*
  hr() {
    return this._newline();
  }
  _newline() {
    console.log('NEWLINE');
    return ' ';
  }

  table(text) {
    return this._plain(text);
  }

  tablerow(text) {
    return this._plain(text);
  }

  tablecell(text) {
    return this._plain(text);
  }

  br() {
    return this._newline();
  }

  html(text) {
    console.log('HTML', text);
    return this._plain(text);
  }

  codespan(text) {
    return this._plain(text);
  }
*/
}

marked.setOptions({renderer: new Renderer()});

export default function md2plain(md) {
  return marked(md.replace(new RegExp('\\n\\n+', 'g'), ' '));
}

