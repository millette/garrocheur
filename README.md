# garrocheur

Garrocheur envoie des messages sur diaspora, twitter et bientôt facebook.
D'ailleurs, si vous utilisez une librairie node pour poster
sur des Pages Facebook, j'apprécierais vos recommandations.

Une fonctionnalité importante et distinctive du garrocheur est de pouvoir
*mapper* les tags et les utilisateurs et d'adapter les messages envoyés
aux différents services.

## Install

```sh
$ git clone ...
$ npm install
```

## Usage

```js
var garrocheur = require('garrocheur');

var account = {
  // ...
};

var message = {
  // ...
};

garrocheur(account, message);
```

## Composantes d'un message
Chaque service supporte un sous-ensemble des composantes.

### Service(s)
Destination où envoyer le message.

### Texte
Plain text ou markdown.

### Hashtags
Un ou plusieurs hashtags.

### Image(s)
Une ou plusieurs images.

### Mentions/destinataires
Les copies-conformes publiques.

### Aspect/groupe
Destiné au public général ou à un groupe particulier d'individus.


## License

AGPLv3.0 © [License AGPLv3.0](LICENSE)


