var Garrocheur = require('..'),
  config = require('./config.json'),
  gar = new Garrocheur('twitter', config.twitter.login);

gar.service('diaspora', config.diaspora.login, config.diaspora.server);

gar.mapAspect('tests', 'twitter');
gar.mapAspect('tests', 'diaspora');
gar.aspects('tests');
gar.mapUser('zigong', ['{zigong ; zigong@framasphere.org}', 'diaspora']);
gar.message('Allo partout @zigong, je teste mon [Garrocheur](https://gitlab.com/facil/garrocheur), **yeah!**', 'markdown');

gar.send(function (err, service, msg) {
  console.log('ERR:', err);
  console.log('service:', service);
  console.log('msg:', msg);
});

