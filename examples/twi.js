var Garrocheur = require('..'),
  config = require('./config.json'),
  gar = new Garrocheur('twitter', config.twitter.login);

gar.public('twitter');
gar.aspects('public');
gar.message('Allo encore @zigong, je teste mon [Garrocheur](https://gitlab.com/facil/garrocheur).', 'markdown');

gar.send(function (err, service, msg) {
  console.log('ERR:', err);
  console.log('service:', service);
  console.log('msg:', msg);
});

console.log('yo');
