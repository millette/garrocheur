var Garrocheur = require('..'),
  config = require('./config.json'),
  gar = new Garrocheur('diaspora', config.diaspora.login, config.diaspora.server);

gar.mapAspect('t2', 'diaspora');
gar.mapAspect('tests', 'diaspora');

gar.aspects('tests');
gar.mapUser('zigong', ['{gongzi ; zigong@framasphere.org}', 'diaspora']);

gar.message('Allo encore @zigong, je teste mon [Garrocheur](https://gitlab.com/facil/garrocheur).', 'markdown');


gar.send(function (err, service, msg) {
  console.log('ERR:', err);
  console.log('service:', service);
  console.log('msg:', msg);
});

console.log('yo');
