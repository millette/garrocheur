import assert from 'assert';
import Service from '../lib/service';

class Blabla extends Service {
  constructor(login, options) {
    super(login, options);
  }

  send() {
    //nothing, but it's there!
  }
}

describe('service', function () {
  it('throw on instanciation (abstract class)', function () {
    assert.throws(function () {
      /*eslint no-new:0*/
      new Service();
    }, Error, 'Could\'t instantiate ABC, great.');
  });

  it('throw on instanciation (missing implementation for abstract method)', function () {
    class Blabla2 extends Service {
      constructor(login, options) {
        super(login, options);
      }
    }
    assert.throws(function () {
      //eslint no-new:0
      new Blabla2();
    }, Error, 'Could\'t instantiate derived class with missing abstract method, great.');
  });

  it('Instanciate complete derived class.', function () {
    let z = new Blabla({username: 'a', password: 'b'});
    assert.equal(z._login.username, 'a', 'got username');
    assert.equal(z._login.password, 'b', 'got password');
  });

  it('limit() method.', function () {
    let z = new Blabla({username: 'a', password: 'b'}), l = z.limit();
    assert.equal(l, 0, 'got limit()');
  });

  it('process() method.', function () {
    let z = new Blabla({username: 'a', password: 'b'}), p = z.process('allo');
    assert.equal(p, 'allo', 'got process()');
  });
});
