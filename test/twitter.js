import assert from 'assert';
import Twitter from '../lib/twitter';
import _Imp from 'twitter';

_Imp.prototype.post = function (where, what, cb) {
  cb(null, what.status, {});
};

/*eslint camelcase: [2, {properties: "never"}]*/
let twLogin = {
  consumer_key: '...',
  consumer_secret: '...',
  access_token_key: '...',
  access_token_secret: '...'
};

describe('twitter', function () {
  it('add twitter service', function () {
    let service = new Twitter(twLogin);
    assert.equal(service.limit(), 140, 'got 140 limit.');
  });

  it('process() method', function () {
    let service = new Twitter(twLogin);
    assert.equal(service.process('**allo**'), '*allo*', 'got processed message.');
  });

  it('send() method', function () {
    let service = new Twitter(twLogin, _Imp);
    service.send('allo', ['public'], function (err, servicename, suc) {
      assert(!err, 'no error, ok');
      assert(suc, 'success, ok');
    });
  });

  it('error on send() method', function () {
    let service;

    _Imp.prototype.post = function (where, what, cb) {
      cb('error');
    };
    service = new Twitter(twLogin, _Imp);
    service.send('allo', ['public'], function (err, servicename, suc) {
      assert(err, 'got error, ok');
      assert(!suc, 'no success, ok');
    });
  });
});
