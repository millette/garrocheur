import assert from 'assert';
import md2plain from '../lib/markdown-to-plain';

describe('markdown-to-plain', function () {
  it('markdown-to-plain paragraph', function () {
    assert.equal(md2plain('bob'), 'bob', 'markdown-to-plain (paragraph)');
  });

  it('markdown-to-plain list', function () {
    assert.equal(
      md2plain('* pierre\n* jean\n* jacques'),
      'pierre, jean, jacques',
      'markdown-to-plain (list)'
    );
  });

  it('markdown-to-plain strong', function () {
    assert.equal(
      md2plain('**bob**'),
      '*bob*',
      'markdown-to-plain (strong)'
    );
  });

  it('markdown-to-plain em', function () {
    assert.equal(
      md2plain('*bob*'),
      '*bob*',
      'markdown-to-plain (em)'
    );
  });

  it('markdown-to-plain heading', function () {
    assert.equal(
      md2plain('# bob'),
      'bob ',
      'markdown-to-plain (heading)'
    );
  });

  it('markdown-to-plain code', function () {
    assert.equal(
      md2plain('    bob-code'),
      'bob-code',
      'markdown-to-plain (code)'
    );
  });

  it('markdown-to-plain blockquote', function () {
    assert.equal(
      md2plain('> bob-quote'),
      'bob-quote',
      'markdown-to-plain (blockquote)'
    );
  });

  it('markdown-to-plain link', function () {
    assert.equal(
      md2plain('[bob](yo)'),
      'yo (bob)',
      'markdown-to-plain (link)'
    );
  });

  it('markdown-to-plain image', function () {
    assert.equal(
      md2plain('![Alt text](/path/to/img.jpg)'),
      '/path/to/img.jpg (Alt text)',
      'markdown-to-plain (image)'
    );
  });

  it('markdown-to-plain del', function () {
    assert.equal(
      md2plain('mot ~~del~~ encore'),
      'mot -del- encore',
      'markdown-to-plain (del)'
    );
  });

  it('markdown-to-plain misc', function () {
    assert.equal(
      md2plain('# mot\n ~~del~~ encore\n\net **plus**!'),
      'mot -del- encore et *plus*!',
      'markdown-to-plain (misc)'
    );
  });

/*
  br()
  hr()

  link(href, title, text)
  image(href, title, text)

  paragraph(text)
  heading(text)
  strong(text)
  em(text)
  list(text)
  listitem(text)
  html(text)
  text(text)

  code(text)
  blockquote(text)
  table(text)
  tablerow(text)
  tablecell(text)
  codespan(text)
  del(text)

  it('markdown-to-plain html', function () {
    assert.equal(
      md2plain('<i>bob666</i>'),
      'bob666',
      'markdown-to-plain (html)'
    );
  });

  it('markdown-to-plain br', function () {
    assert.equal(
      md2plain('bob\nyo'),
      'bob yo',
      'markdown-to-plain (br)'
    );
  });

  it('markdown-to-plain br (2)', function () {
    assert.equal(
      md2plain('bob<br>yo'),
      'bob yo',
      'markdown-to-plain (br 2)'
    );
  });
*/
});
