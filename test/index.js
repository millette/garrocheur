import assert from 'assert';
import Garrocheur from '../lib';
import DiasporaImp from 'node-diaspora';
import TwitterImp from 'twitter';

TwitterImp.prototype.post = function (where, what, cb) {
  cb(null, what.status, {});
};

/*eslint camelcase: [2, {properties: "never"}]*/
const twLogin = {
    consumer_key: '...',
    consumer_secret: '...',
    access_token_key: '...',
    access_token_secret: '...'
  },
  dLogin = { username: 'joe', password: 'blo' }, dServer = 'framasphere';

describe('garrocheur', function () {
  it('throw on unsupported (badass) service', function () {
    let gar = new Garrocheur();
    assert.throws(function () {
      gar.service('badass', twLogin);
    }, Error, 'unsupported service.');
  });

  it('add twitter service', function () {
    let gar = new Garrocheur('twitter', twLogin);
    assert.equal(Object.keys(gar._services).length, 1, 'we expected to find 1 service.');
  });

  it('add twitter twice', function () {
    let gar = new Garrocheur('twitter', twLogin);
    gar.service('twitter', twLogin);
    assert.equal(Object.keys(gar._services).length, 1, 'we expected to find 1 service.');
  });

  it('add twitter service on construction', function () {
    let gar = new Garrocheur('twitter', twLogin);
    assert.equal(Object.keys(gar._services).length, 1, 'we expected to find 1 service.');
  });

  it('add twitter service on construction (2)', function () {
    let gar = new Garrocheur('twitter', twLogin);
    assert.equal(Object.keys(gar._services).length, 1, 'we expected to find 1 service.');
  });

  it('add twitter and diaspora services', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.service('diaspora', dLogin, dServer);
    assert.equal(Object.keys(gar._services).length, 2, 'we expected to find both services.');
  });

  it('add twitter and diaspora services (2)', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.service('diaspora', dLogin, dServer);
    assert.equal(Object.keys(gar._services).length, 2, 'we expected to find both services.');
  });

  it('throws when mapping a user to an unknown service', function () {
    let gar = new Garrocheur();
    assert.throws(function () {
      gar.mapUser('joe', 'twitter');
    }, Error, 'Throw when service is not yet defined.');
  });

  it('map a user to a known service', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.mapUser('joe', 'twitter');
    assert.equal(Object.keys(gar._users).length, 1, 'we expected to find 1 user.');
  });

  it('map a user to a known service, long form', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.mapUser('joe', ['joe', 'twitter']);
    assert.equal(Object.keys(gar._users).length, 1, 'we expected to find 1 user.');
  });

  it('map two users to a known service', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.mapUser('joe', 'twitter');
    gar.mapUser('blo', ['blo', 'twitter']);
    assert.equal(Object.keys(gar._users).length, 2, 'we expected to find 2 users.');
  });

  it('map two users to a known service', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.service('diaspora', dLogin, dServer);
    gar.mapUser('joe', 'twitter');
    gar.mapUser('joe', 'diaspora');
    assert.equal(Object.keys(gar._users).length, 1, 'we expected to find 1 user.');
  });

  it('send message with aspects (2)', function () {
    let gar = new Garrocheur('twitter', twLogin);
    assert.throws(function () {
      gar.mapAspect('twitter');
    }, Error, 'Throw when calling mapAspect with wrong arguments.');
  });

  it('throws when mapping an aspect to an unknown service', function () {
    let gar = new Garrocheur();
    assert.throws(function () {
      gar.mapAspect('joe', 'twitter');
    }, Error, 'Throw when service is not yet defined.');
  });

  it('map an aspect to a known service', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.mapAspect('joe', 'twitter');
    assert.equal(Object.keys(gar._aspects).length, 1, 'we expected to find 1 aspect.');
  });

  it('map an aspect to a known service (2)', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.service('diaspora', dLogin, dServer);
    gar.mapAspect('joe', 'twitter');
    gar.mapAspect('joe', 'diaspora');
    assert.equal(Object.keys(gar._aspects).length, 1, 'we expected to find 1 aspect.');
  });

  it('map an aspect to a known service, long form', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.mapAspect('joe', ['joe', 'twitter']);
    assert.equal(Object.keys(gar._aspects).length, 1, 'we expected to find 1 aspect.');
  });

  it('map an aspect to an unknown service', function () {
    let gar = new Garrocheur();
    assert.throws(function () {
      gar.mapAspect('joe', 'twitter');
    }, Error, 'unknown service.');
  });

  it('map two aspects to a known service', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.mapAspect('joe', 'twitter');
    gar.mapAspect('blo', 'twitter');
    assert.equal(Object.keys(gar._aspects).length, 2, 'we expected to find 2 users.');
  });

  it('throws when mapping publicly an aspect to an unknown service', function () {
    let gar = new Garrocheur();
    assert.throws(function () {
      gar.public('friends', 'twitter');
    }, Error, 'Throw when service is not yet defined.');
  });

  it('map an aspect publicly to a known service', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    gar.public('friends', 'twitter');
    assert.equal(Object.keys(gar._aspects).length, 1, 'we expected to find 1 aspect.');
  });

  it('set message', function () {
    let gar = new Garrocheur();
    gar.message('allo');
    assert.equal(gar._message, 'allo', 'message compris.');
  });

  it('set markdown iso-8859 message', function () {
    let gar = new Garrocheur();
    gar.message('# allo', 'markdown; charset=ISO-8859-15');
    assert.equal(gar._message, '# allo', 'message compris.');
    assert.equal(gar._mimetype, 'text/markdown; charset=ISO-8859-15', 'format markdown iso-8859-15.');
  });

  it('set markdown message', function () {
    let gar = new Garrocheur();
    gar.message('# allo', 'markdown');
    assert.equal(gar._message, '# allo', 'message compris.');
    assert.equal(gar._mimetype, 'text/markdown; charset=UTF-8', 'format markdown.');
  });

  it('set markdown message, long form', function () {
    let gar = new Garrocheur();
    gar.message('# allo', 'text/markdown');
    assert.equal(gar._message, '# allo', 'message compris.');
    assert.equal(gar._mimetype, 'text/markdown; charset=UTF-8', 'format markdown.');
  });

  it('set aspect', function () {
    let gar = new Garrocheur();
    gar.aspects('allo');
    assert.equal(gar._aspect.length, 1, 'aspect compris.');
  });

  it('set aspects', function () {
    let gar = new Garrocheur();
    gar.aspects(['allo', 'joe']);
    assert.equal(gar._aspect.length, 2, 'aspects compris.');
  });

  it('send message, callback test', function () {
    let gar = new Garrocheur('twitter', twLogin);
    gar.message('allo');
    gar.send(function (err) {
      assert.equal(
        err,
        'error: no known aspects on twitter.',
        'message non envoyé'
      );
    });
  });

  it('send message with aspect, callback test', function () {
    let gar = new Garrocheur('twitter', twLogin, TwitterImp);
    gar.public('twitter');
    gar.message('allo');
    gar.send(function (err, service, msg) {
      assert(!err, 'ok, pas d\'erreur.');
      assert.equal(service, 'twitter', 'message envoyé sur twitter');
      assert.equal(msg[2], 'allo', 'message envoyé sur twitter: allo');
      assert.equal(msg[3], '', 'message envoyé sur twitter, c\'est tout.');
    });
  });

  it('send message with aspects (sending one), callback test', function () {
    let gar = new Garrocheur('twitter', twLogin, TwitterImp);
    gar.service('diaspora', dLogin, dServer);
    gar.public('friends', 'twitter');
    gar.mapAspect('family', 'diaspora');
    gar.message('allo');
    gar.send(function (err, service, msg) {
      if (err) {
        assert.equal(
          err,
          'error sending to diaspora, no aspects to send to.',
          'ok, ne peut envoyer sur diaspora, aspects ne correspondent pas.');
      } else {
        assert.equal(service, 'twitter', 'message envoyé sur twitter');
        assert.equal(msg[2], 'allo', 'message envoyé');
      }
    });
  });

  it('send long markdown message with aspects, callback test', function () {
    let r = 25, e = '', str = '', gar = new Garrocheur('twitter', twLogin);

    DiasporaImp.prototype.connect = function (cb) {
      cb(null, 'yup');
    };

    DiasporaImp.prototype.postStatusMessage = function (msg, aspects, cb) {
      cb(null, 'yup');
    };

    gar.service('diaspora', dLogin, dServer, DiasporaImp);
    gar.public('friends', 'twitter');
    gar.mapAspect(Garrocheur.PUBLIC, 'diaspora');
    while (r--) {
      e += 'le **e** ';
    }
    e = e + 'f';
    str = e + ' [allo](link)';
    gar.message(str, 'markdown');

    gar.send(function (err, servicename, ok) {
      assert(!err, 'ok, pas d\'erreur.');
      if ('twitter' === servicename) {
        assert(ok, 'message envoyé (twitter)');
      } else if ('diaspora' === servicename) {
        assert(ok, 'message envoyé (diaspora)');
      } else {
        assert(false, 'quel est ce service?');
      }
    });
  });

/* removed for now
  it('send long message with aspect, callback test', function () {
    let r = 139, e = '', str = '', gar = new Garrocheur('twitter', twLogin);
    gar.public('twitter');
    while (r--) {
      e += 'e';
    }
    e = e + 'f';
    str = e + 'allo';
    gar.message(str);
    gar.send(function (err, service, msg) {
      assert(!err, 'ok, pas d\'erreur.');
      assert.equal(msg[1], 'twitter', 'message envoyé');
      assert.equal(msg[2], e, 'message envoyé');
      assert.equal(msg[3], 'allo', 'message envoyé');
    });
  });

  it('send message with aspects (sending both), callback test', function () {
    let gar = new Garrocheur('twitter', twLogin);

    DiasporaImp.prototype.connect = function (cb) {
      cb(null, 'yup');
    };

    gar.service('diaspora', dLogin, dServer, DiasporaImp);
    gar.public('friends', 'twitter');
    gar.mapAspect(Garrocheur.PUBLIC, 'diaspora');
    gar.message('allo');
    try {
      gar.send(function (err, service) {
        assert(!err, 'ok, pas d\'erreur.');
        assert.notEqual(['twitter', 'diaspora'].indexOf(service), -1, 'message envoyé à twitter');
      });
    } catch(e) {
      console.log('OUPSY, got', e.toString(), e.message, e.name);
    }
  });

  it('send long message with aspects, callback test', function () {
    let r = 139, e = '', str = '', gar = new Garrocheur('twitter', twLogin);
    gar.service('diaspora', dLogin, dServer);
    gar.public('friends', 'twitter');
    gar.mapAspect(Garrocheur.PUBLIC, 'diaspora');
    while (r--) {
      e += 'e';
    }
    e = e + 'f';
    str = e + 'allo';
    gar.message(str);
    gar.send(function (err, service, msg) {
      assert(!err, 'ok, pas d\'erreur.');
      if ('twitter' === service) {
        assert.equal(msg[2], e, 'message envoyé (twitter)');
        assert.equal(msg[3], 'allo', 'message envoyé (twitter)');
      } else if ('diaspora' === service) {
        assert.equal(msg[2], str, 'message envoyé (diaspora)');
        assert.equal(msg[3], '', 'message envoyé (diaspora)');
      } else {
        assert(false, 'quel est ce service?');
      }
    });
  });
*/

  it('throw on send message', function () {
    let gar = new Garrocheur();
    gar.service('twitter', twLogin);
    assert.throws(function () {
      gar.send();
    }, Error, 'No message to send.');
  });

  it('throw on send message (2)', function () {
    let gar = new Garrocheur();
    assert.throws(function () {
      gar.send();
    }, Error, 'Send message nowhere.');
  });

  it('throw on mapUsers', function () {
    let gar = new Garrocheur();
    assert.throws(function () {
      gar.mapUsers('twitter');
    }, Error, 'Map users for unknown service.');
  });

  it('mapUsers, no message', function () {
    let gar = new Garrocheur('twitter', twLogin),
      msg = gar.mapUsers('twitter');
    assert.equal(msg, false, 'No message, but we mapped our users ;-)');
  });

  it('mapUsers, with message, no users', function () {
    let gar = new Garrocheur('twitter', twLogin),
      msg;
    gar.message('Allo');
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo', 'With message, we mapped our users ;-)');
  });

  it('mapUsers, with message and user', function () {
    let gar = new Garrocheur('twitter', twLogin),
      msg;
    gar.message('Allo @joe ça va?');
    gar.mapUser('joe', ['joseph', 'twitter']);
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo @joseph ça va?', 'With message, we mapped our users ;-)');

    gar.message('Allo @joey');
    gar.mapUser('joe', ['joseph', 'twitter']);
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo @joey', 'With message, we mapped our users ;-)');

    gar.message('Allo @joe');
    gar.mapUser('joe', ['joseph', 'twitter']);
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo @joseph', 'With message, we mapped our users ;-)');

    gar.message('Allo @joe, ça va @joe?');
    gar.mapUser('joe', ['joseph', 'twitter']);
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo @joseph, ça va @joseph?', 'With message, we mapped our users ;-)');
  });

  it('mapUsers, with message and users', function () {
    let gar = new Garrocheur('twitter', twLogin),
      msg;
    gar.service('diaspora', dLogin, dServer);
    gar.message('Allo @joe ça va?');
    gar.mapUser('joe', ['joeblo', 'diaspora']);
    gar.mapUser('joe', ['joseph', 'twitter']);
    gar.mapUser('bob', ['bo', 'twitter']);
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo @joseph ça va?', 'With message, we mapped our users ;-)');

    gar.message('Allo @bob, @joey');
    gar.mapUser('joe', ['joseph', 'twitter']);
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo @bo, @joey', 'With message, we mapped our users ;-)');

    gar.message('Allo @joe et @bob');
    gar.mapUser('joe', ['joseph', 'twitter']);
    msg = gar.mapUsers('twitter');
    assert.equal(msg, 'Allo @joseph et @bo', 'With message, we mapped our users ;-)');
  });
});
