import assert from 'assert';
import Diaspora from '../lib/diaspora';
import _Imp from 'node-diaspora';

//let dUsername = 'joe', dPassword = 'blo', dServer = 'framasphere';
let dLogin = { username: 'joe', password: 'blo' }, dServer = 'framasphere';

describe('diaspora', function () {
  it('add diaspora service with options', function () {
    let service = new Diaspora(dLogin, dServer);
    assert.equal(service._options.server, dServer, 'got server.');
  });

  it('add diaspora service with mock', function () {
    let service;

    _Imp.prototype.connect = function (cb) {
      cb(null, 'yup');
    };

    service = new Diaspora(dLogin, dServer, _Imp);
    service._connect(function (err, suc) {
      assert.equal(service._options.server, dServer, 'got server.');
      assert.equal(suc, 'yup', 'got response thru callback.');
      assert(!err, 'no error thru callback, we\'re good.');
    });
  });

  it('throw on add diaspora service with mock and bad server', function () {
    assert.throws(function () {
      /*eslint no-new:0*/
      new Diaspora(dLogin, '', _Imp);
    }, Error, 'User, password and pod are necessary.');
  });

/*
  it('callback error on add diaspora service with mock and bad server', function () {
    let service;

    _Imp.prototype.connect = function (cb) {
      cb('Bad server');
    };

    service = new Diaspora(dUsername, dPassword, '.', _Imp);
    service._connect(function (err, suc) {
      assert.equal(service._options.server, '.', 'got server.');
      assert(!suc, 'no success response, as expected.');
      assert.equal(err, 'Bad server', 'error response, as expected.');
    });
  });
*/

  it('throw on connect', function () {
    let service;

    _Imp.prototype.connect = function () {
      throw new Error('Mwahaha!');
    };

    service = new Diaspora(dLogin, '.', _Imp);
    assert.throws(function () {
      service._connect();
    }, Error, 'Mwahaha!');
  });

  it('send with mock', function () {
    let service;

    _Imp.prototype.connect = function (cb) {
      cb(null, 'yup');
    };

    _Imp.prototype.postStatusMessage = function (msg, aspects, cb) {
      cb(null, 'yup');
    };

    service = new Diaspora(dLogin, dServer, _Imp);
    service.send('allo', ['test'], function (err, servicename, suc) {
      assert.equal(service._options.server, dServer, 'got server.');
      assert.equal(suc, 'yup', 'got response thru callback.');
      assert(!err, 'no error thru callback, we\'re good.');
    });
  });

  it('send with mock, bad server', function () {
    let service;

    _Imp.prototype.connect = function (cb) {
      cb('err');
    };

    _Imp.prototype.postStatusMessage = function (msg, aspects, cb) {
      cb(null, 'yup');
    };

    service = new Diaspora(dLogin, '.', _Imp);
    service.send('allo', ['test'], function (err, servicename, suc) {
      assert(!suc, 'no success response, as expected.');
      assert(err, 'error response, as expected.');
    });
  });
});
