var gulp = require('gulp'),
  isparta = require('isparta'),
  $ = require('gulp-load-plugins')();

// Initialize the babel transpiler so ES2015 files gets compiled
// when they're loaded
require('babel-core/register');

function missingTypeof(file) {
  var regex = /.+('|")undefined(?:\1).+/g,
    lines = file.contents.toString().match(regex);
  if (lines) {
    lines.forEach(function (v) {
      if (-1 === v.indexOf('typeof')) {
        console.log('warning, missing typeof comparing to "undefined" in ' + file.path.replace(file.base, '') + ' on line');
        console.log(v);
      }
    });
  }
}

gulp.task('static', function () {
  return gulp.src('**/*.js')
    .pipe($.excludeGitignore())
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.tap(missingTypeof))
    .pipe($.eslint.failAfterError());
});

gulp.task('nsp', function (cb) {
  $.nsp('package.json', cb);
});

gulp.task('pre-test', function () {
  return gulp.src('lib/**/*.js')
    .pipe($.istanbul({
      includeUntested: true,
      instrumenter: isparta.Instrumenter
    }))
    .pipe($.istanbul.hookRequire());
});

gulp.task('test', ['pre-test'], function (cb) {
  var mochaErr;

  gulp.src('test/**/*.js')
    .pipe($.plumber())
    .pipe($.mocha({reporter: 'spec'}))
    .on('error', function (err) {
      mochaErr = err;
    })
    .pipe($.istanbul.writeReports())
    .on('end', function () {
      cb(mochaErr);
    });
});

gulp.task('examples-config', function () {
  return gulp.src('examples/**.json')
    .pipe($.stripJsonComments())
    .pipe(gulp.dest('dist/examples'));
});

gulp.task('examples', ['examples-config', 'babel'], function () {
  return gulp.src('examples/**.js')
    .pipe($.babel())
    .pipe(gulp.dest('dist/examples'));
});

gulp.task('babel', function () {
  return gulp.src(['lib/**/*.js'])
    .pipe($.babel())
    .pipe(gulp.dest('dist'));
});

gulp.task('prepublish', ['nsp', 'babel']);
gulp.task('default', ['static', 'test']);
